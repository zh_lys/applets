## git 使用



#### 1、创建仓库

![image-20221020220611121](image\image-20221020220611121.png)

#### 2、初始化仓库

![image-20221020220726070](image\image-20221020220726070.png)

#### 3、将本地仓库与远程仓库绑定

* 3-1、复制下面代码

```bash
git remote add origin https://gitee.com/zh_lys/my-test.git
```

* 3-2、在项目文件夹内打开cmd

#### 4、git 操作(项目文件夹下，打开PowerShell)

* 添加文件到缓存区

  ```bash
  git add .
  ```

* 在本地做一次提交

  ```bash
  git commit -m "******"
  ```

* 检查源代码状态

  ```bash
  git status
  ```

* 绑定远程仓库

  ```bash
  git remote add origin https://gitee.com/zh_lys/my-test.git
  ```

* 提交代码到远程仓库

  ```bash
  git push -u origin "master"
  ```

  此时会弹出一个需要填写用户名密码的提交框，输入用户名和密码后点击确认即可。之后再提交就不用再输入用户名和密码了。 

* 新功能开发时，最好先创建分支，创建分支命令如下

  ```bash
  git checkout -b login
  # 创建了一个名为login的分支，并且切换至login分支上
  ```

* 查看分支

  ```bash
  git branch
  # 查看当前所有分支，带*号的表示当前仓库所在分支
  ```

* 合并分支

  ```bash
  # 1、切换分支，要合并到哪个分支，就要先切换到哪个分支
  git checkout master
  # 2、查看所在分支
  git branch
  # 3、合并分支，要合并哪个分支，就merge哪个分支
  git merge login
  # 4、现在就将本地的主分支合并了login分支，提交主分支到远程仓库
  git push
  # 5、现在本地有两个分支（master，login），云端只有一个分支（master），将本地login分支也推送至云端仓库，先切换至login分支
  git checkout login
  # 6、查看分支
  git branch
  # 7、首次提交分支到远程仓库，不能直接使用git push，需要使用的命令如下
  git push -u origin login
  # 上面命令的意思是说，将本地仓库的login分支，推送至origin仓储下名为login的分支
  
  
  ```

  

* 

