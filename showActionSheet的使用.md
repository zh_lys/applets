# wx.showActionSheet(Object object)

描述：显示操作菜单

示例代码：

```javascript
wx.showActionSheet({
      itemList: ['分享到QQ','分享到微信','分享到朋友圈'],
      success(res){
        console.log(res)
      }
})
```

变形1：

```javascript
async onShare(event){
   const result = await wx.showActionSheet({
      itemList: ['分享到QQ','分享到微信','分享到朋友圈']
    })
   console.log(result)
}
```

