#### 1、居中处理

----

文字居中：`text-align`

水平居中：`line-height`（与父元素高度一致）

垂直居中页面多元素一次性垂直居中：用flex布局-水平居中父元素，`display: flex;flex-direction: column, align-items: center;`垂直居中子元素 `line-height（与父元素高度一致）`

例如：

![image-20220927221143781](C:\Users\45998\AppData\Roaming\Typora\typora-user-images\image-20220927221143781.png)

代码：

```html
<view class="post-author-date">
    <image catch:tap="onMaxImage" class="post-author" src="{{post.avatar}}"></image>
    <text class="post-date">{{post.date}}</text>
</view>
```



```css
.post-author-date{
  display: flex;
  margin: 10rpx 0 20rpx 10rpx;
  flex-direction: row;
  align-items: center;
}

.post-author-date{
  display: flex;
  margin: 10rpx 0 20rpx 10rpx;
  flex-direction: row;
  align-items: center;
}
```



