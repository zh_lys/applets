#### 1、showToast的使用

```javascript
// 轻提示
wx.showToast({
    title:'收藏成功',
    duration: 2000 // 停留时长
})
```

样例：

![image-20220929222618128](C:\Users\45998\AppData\Roaming\Typora\typora-user-images\image-20220929222618128.png)

#### 2、showModel的使用

```javascript
wx.showModal({
      title: '是否收藏文章',
      cancelText: 'A',
      confirmText:'B',

})
```

样例：

![image-20220929223216860](C:\Users\45998\AppData\Roaming\Typora\typora-user-images\image-20220929223216860.png)

```javascript
wx.showModal({
      title: '是否收藏文章',
      showCancel: false,
      success: function(res){
        console.log(res)
      },
      fail:function(res){
        console.log(res)
      }
})
// 点击确认按钮
success：{errMsg: "showModal:ok", cancel: false, confirm: true, content: null}
fail: {errMsg: "showModal:ok", cancel: true, confirm: false}
```

样例：

![image-20220929223605536](C:\Users\45998\AppData\Roaming\Typora\typora-user-images\image-20220929223605536.png)

showModal变形版1：

```javascript
onCollect(event){
    const _this = this
    wx.showModal({
      title: _this.data.collected?'是否取消收藏':'是否收藏文章',
      //showCancel: false,
      success: function(res){
        if(res.confirm){
          const posts_collected = wx.getStorageSync('posts_collected')
          // 从属性上获取
          const postId = event.currentTarget.dataset.id
          console.log(postId)
          _this.setData({
            collected:!_this.data.collected
          })
          posts_collected[postId] = _this.data.collected
  
          wx.setStorageSync('posts_collected', posts_collected)
        }
      }
    })
  }
```

showModal变形版2：

```javascript
async onCollect(event){
    const result = await wx.showModal({
      title: this.data.collected?'是否取消收藏':'是否收藏文章'
    })
    if(result.confirm){
      const posts_collected = wx.getStorageSync('posts_collected')
      // 从属性上获取
      const postId = event.currentTarget.dataset.id
      console.log(postId)
      this.setData({
        collected:!this.data.collected
      })
      posts_collected[postId] = this.data.collected

      wx.setStorageSync('posts_collected', posts_collected)
    }
}
```

